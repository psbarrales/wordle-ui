import styled from 'styled-components'

const Content = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
`

const Layout = ({ children }) => {
  return <Content>{children}</Content>
}

export default Layout
