import styled from 'styled-components'

const Content = styled.div`
  width: 4rem;
  height: 4rem;
  border: 1px solid rgb(150, 150, 150);
  background: ${(props) =>
    props.state === 0
      ? 'transparent'
      : props.state === 1
      ? '#07bc0c'
      : '#f1c40f'};
  font-size: 3rem;
  text-align: center;
  line-height: 4rem;
  text-transform: uppercase;
  color: white;
  font-weight: bold;
  user-select: none;
`

const InputBox = ({ value, state }) => {
  return <Content state={state}>{value}</Content>
}

export default InputBox
