import { useEffect, useState } from 'react'
import styled from 'styled-components'
import { Button, notification } from 'antd'
import InputBox from './InputBox'
import useWords from '../../hooks/useWords'

const Content = styled.div`
  flex: 1;
  background: rgba(45, 45, 45);
  width: 100%;
  padding: 2rem 1rem;
  > div {
    display: flex;
    justify-content: center;
  }
  .button-wrapper {
    margin-top: 2rem;
  }
`

const Game = () => {
  const words = useWords()
  const validKeys = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZENTERBACKSPACE'
  const [word, setWord] = useState()
  const [keyUpPressed, setKeyUpPressed] = useState()
  const [position, setPosition] = useState([0, 0])
  const [isFinished, setIsFinished] = useState(false)
  const [games, setGames] = useState([
    [
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
    ],
    [
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
    ],
    [
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
    ],
    [
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
    ],
    [
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
    ],
    [
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
      { value: '', state: 0 },
    ],
  ])

  const validate = () => {
    const pos = JSON.parse(JSON.stringify(position))
    const game = JSON.parse(JSON.stringify(games[pos[0]]))
    console.log(game, game.map((g) => g.value !== '').indexOf(false))
    if (game.map((g) => g.value !== '').indexOf(false) < 0) {
      validateGame(game, pos)
      if (pos[0] < 5) {
        pos[0] += 1
        pos[1] = 0
      }
      setPosition(pos)
    }
  }

  const validateGame = async (game, pos) => {
    const inputWord = game.map((g) => g.value).join('')
    if (await words.validate(inputWord)) {
      game.forEach((g, i) => {
        const value = g.value
        if (value === word[i]) {
          game[i].state = 1
        } else if (word.indexOf(value) >= 0) {
          game[i].state = 2
        }
      })
      games[pos[0]] = game
      setIsFinished(
        pos[0] === 5 || game.map((g) => g.state === 1).indexOf(false) < 0,
      )
      setGames(games)
      return true
    } else {
      notification.error({
        message: 'Palabra no valida',
        description: 'La palabra no es una palabra en español.',
      })
      return false
    }
  }

  const getWord = async () => {
    setWord(await words.random())
  }

  useEffect(() => {
    if (keyUpPressed && !isFinished) {
      const key = keyUpPressed.key.toUpperCase()
      const isValidKey = validKeys.indexOf(key) >= 0
      const pos = JSON.parse(JSON.stringify(position))
      let game = JSON.parse(JSON.stringify(games[position[0]]))
      const goBack = () => {
        if (pos[1] > 0) {
          pos[1] += -1
        }
      }
      const goForward = () => {
        if (pos[1] < 4) {
          pos[1] += 1
        }
      }
      const onLetter = () => {
        game[pos[1]].value = key
        goForward()
      }
      const onBackSpace = () => {
        if (game[pos[1]].value !== '') {
          game[pos[1]].value = ''
          goBack()
        } else {
          goBack()
          game[pos[1]].value = ''
        }
      }
      const onEnter = async () => {
        if (await validateGame(game, pos)) {
          if (pos[0] < 5) {
            pos[0] += 1
            pos[1] = 0
          }
          game = JSON.parse(JSON.stringify(games[pos[0]]))
          games[pos[0]] = game
          setPosition(pos)
          setGames(games)
        }
      }
      if (isValidKey) {
        if (
          key !== 'ENTER' &&
          key !== 'BACKSPACE' &&
          pos[1] <= 4 &&
          game[pos[1]].value === ''
        ) {
          onLetter()
        }
        if (key === 'BACKSPACE' && pos[1] >= 0) {
          onBackSpace()
        }
        if (key === 'ENTER' && pos[1] === 4) {
          return onEnter()
        }
      }
      games[pos[0]] = game
      setPosition(pos)
      setGames(games)
    }
  }, [keyUpPressed])

  useEffect(() => {
    window.addEventListener('keyup', setKeyUpPressed)
    return () => {
      window.removeEventListener('keyup', setKeyUpPressed)
    }
  }, [])

  useEffect(() => {
    if (!word) {
      getWord()
    }
  }, [word])

  return (
    <Content>
      {games.map((game, i) => (
        <div key={i}>
          {game.map((g, j) => (
            <InputBox key={j} value={g.value} state={g.state} />
          ))}
        </div>
      ))}
      <div className="button-wrapper" onClick={validate}>
        <Button>Continuar</Button>
      </div>
    </Content>
  )
}

export default Game
