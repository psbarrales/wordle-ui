import styled from 'styled-components'

const Content = styled.div`
  width: 100%;
  height: 4rem;
  font-size: 1.25rem;
  background: rgba(35, 35, 35);
  text-align: center;
  line-height: 4rem;
  color: white;
  text-transform: uppercase;
  font-weight: bold;
  border-bottom: 1px solid rgba(200, 200, 200, 0.5);
`

const TabBar = () => {
  return <Content>Wordle</Content>
}

export default TabBar
