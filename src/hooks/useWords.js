import axios from 'axios'

const useWords = () => {
  const random = async () => {
    const { data } = await axios.get(
      `${process.env.NEXT_PUBLIC_API_HOST}/v1/words/random`,
    )
    return data
  }

  const validate = async (word) => {
    try {
      await axios.post(
        `${process.env.NEXT_PUBLIC_API_HOST}/v1/words/validate`,
        { word },
      )
      return true
    } catch (err) {
      return false
    }
  }

  return {
    random,
    validate,
  }
}

export default useWords
