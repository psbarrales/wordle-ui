import Game from '../components/Game'
import Layout from '../components/Layout'
import TabBar from '../components/TabBar'

const HomePage = () => {
  return (
    <Layout>
      <TabBar />
      <Game />
    </Layout>
  )
}

export default HomePage
